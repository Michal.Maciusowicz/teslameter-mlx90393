#include <Wire.h>
#include <MLX90393.h>
#include "ssd1306.h"
#include <SPI.h>
#include <SD.h>

File plikSD;
const byte szerokosc = 128;
const byte wysokosc = 64;
uint8_t oled_buf[szerokosc * wysokosc / 8];
const byte buttonPin1 = 6;
const byte buttonPin2 = 4;
const byte buttonPin3 = 5;
const byte buttonPin4 = 3;
float Xcon;
float Ycon;
float Zcon;
int GainAct = 0;
//From https://github.com/tedyapo/arduino-MLX90393 by Theodore Yapo
MLX90393 mlx;
MLX90393::txyz data; //Create a structure, called data, of four floats (t, x, y, and z)
MLX90393::txyz DataSD;
// SSD1306_string(x,y,tekst,wielkosc,tryb,buffor)
void StartUpHall()
{
  byte status = mlx.begin(0, 0, A3);
  Serial.print("Start status: 0x");
  if (status < 0x10)
    Serial.print("0"); //Pretty output
  Serial.println(status, BIN);
  mlx.setGainSel(7);
  GainAct = 1;
  mlx.setResolution(0, 0, 0); //x, y, z
  mlx.setOverSampling(1);
  mlx.setDigitalFiltering(1);
  mlx.setTemperatureCompensation(1);
  mlx.setOffsets(0x8000, 0x8008, 0x802E);
}
void Menu()
{
  SSD1306_clear(oled_buf);
  byte state = 1;
  SSD1306_display(oled_buf);
  SSD1306_string(50, 0, "MENU", 12, 0, oled_buf);
  SSD1306_string(10, 20, "1) Meas.", 12, 3, oled_buf);
  SSD1306_string(10, 30, "2) Read from SD", 12, 3, oled_buf);
  SSD1306_string(10, 40, "3) Settings", 12, 3, oled_buf);
  SSD1306_display(oled_buf);
  while (true)
  {
    delay(200);
    if (digitalRead(buttonPin1) == HIGH)
    {
      if (state < 3)
        state++;
      else
        state = 1;
      MenuString(state);
    }
    else if (digitalRead(buttonPin2) == HIGH)
    {
      if (state == 1)
        state = 3;
      else
        state--;
      MenuString(state);
    }
    else
    {
      delay(200);
      if (digitalRead(buttonPin3) == HIGH)
      {
        if (state == 1)
          printOLED();
        if (state == 2)
          ReadFromFile();
        if (state == 3)
          Settings();
      }
    }
  }
}
void ReadFromFile()
{
  SSD1306_clear(oled_buf);
  SSD1306_string(20, 0, "Read from SD", 12, 0, oled_buf);
  SSD1306_display(oled_buf);
  plikSD = SD.open("opis.txt");
  delay(100);
  while (true)
  {
    float valueX = 0;
    float valueY = 0;
    float valueZ = 0;
    if (digitalRead(buttonPin1) == HIGH)
    { 
      byte status = 0;
      byte index = 0;
      char buffX[8];
      while (plikSD.available())
      {
        char aChar = plikSD.read();
        //Serial.println(index);
        Serial.print(aChar);
        if (aChar != '\n' && aChar != '\r' && aChar !='\t')
        {
          buffX[index++] = aChar;
          buffX[index] = '\0'; // NULL terminate the array
        }       
        else
        {
          if (strlen(buffX) > 0 && status == 0)
          {
            valueX = atof(buffX);
          }
          if (strlen(buffX) > 0 && status == 1)
          {
            valueY = atof(buffX);
          }
          if (strlen(buffX) > 0 && status == 2)
          {
            valueZ = atof(buffX);
          }
          buffX[0] = '\0';
          index = 0;
          status++;
          if(status == 3)
          {
            status == 0;
          }
        }
      }
      delay(100);
      SSD1306_clear(oled_buf);
      ReadFromFileMenu(valueX, valueY,valueZ);
      delay(2000);
      SSD1306_clear(oled_buf);
    }
    if(digitalRead(buttonPin4) == HIGH)
      break;
  }
  plikSD.close();
  Menu();
}
void ReadFromFileMenu(float valueX, float valueY, float valueZ)
{
  delay(500);
  SSD1306_clear(oled_buf);
  SSD1306_string(20, 0, "Read from SD", 12, 0, oled_buf);
  char charX[8];
  sprintf(charX, "%5.2f", valueX);
  SSD1306_string(40, 20, charX, 12, 3, oled_buf);
  SSD1306_string(100, 20, "uT", 12, 3, oled_buf);
  char charY[8];
  sprintf(charY, "%5.2f", valueY);
  SSD1306_string(40, 30, charY, 12, 3, oled_buf);
  SSD1306_string(100, 30, "uT", 12, 3, oled_buf);
  char charZ[8];
  sprintf(charZ, "%5.2f", valueZ);
  SSD1306_string(40, 40, charZ, 12, 3, oled_buf);
  SSD1306_string(100, 40, "uT", 12, 3, oled_buf);
  SSD1306_display(oled_buf);
}
  void Settings()
  {
    SSD1306_clear(oled_buf);
    byte state = 1;
    MenuSettings(1);
    SSD1306_string(50, 0, "SETTINGS", 12, 0, oled_buf);
    while (true)
    {

      delay(200);
      if (digitalRead(buttonPin1) == HIGH)
      {
        if (state < 2)
          state++;
        else
          state = 1;
        MenuSettings(state);
      }
      else if (digitalRead(buttonPin2) == HIGH)
      {
        if (state < 1)
          state = 2;
        else
          state--;
        MenuSettings(state);
      }
      else if (digitalRead(buttonPin4) == HIGH)
        break;
      else if (digitalRead(buttonPin3) == HIGH)
      {
        if (state == 1)
        {
          Gain();
        }
        if (state == 2)
        {
          Res();
        }
      }
    }
    Menu();
  }
  void MenuSettings(int setStatus)
  {
    SSD1306_clear(oled_buf);
    SSD1306_string(50, 0, "SETTINGS", 12, 0, oled_buf);
    switch (setStatus)
    {
    case 1:
      SSD1306_string(10, 20, "1) Gain", 12, 0, oled_buf);
      SSD1306_string(10, 30, "2) Resolution", 12, 3, oled_buf);
      SSD1306_display(oled_buf);
      break;
    case 2:
      SSD1306_string(10, 20, "1) Gain", 12, 3, oled_buf);
      SSD1306_string(10, 30, "2) Resolution", 12, 0, oled_buf);
      SSD1306_display(oled_buf);
      break;
    }
  }
  void MenuString(int set)
  {
    switch (set)
    {
    case 1:
      SSD1306_string(10, 20, "1) Meas.", 12, 0, oled_buf);
      SSD1306_string(10, 30, "2) Read from SD", 12, 3, oled_buf);
      SSD1306_string(10, 40, "3) Settings", 12, 3, oled_buf);
      SSD1306_display(oled_buf);
      break;
    case 2:
      SSD1306_string(10, 20, "1) Meas.", 12, 1, oled_buf);
      SSD1306_string(10, 30, "2) Read from SD", 12, 0, oled_buf);
      SSD1306_string(10, 40, "3) Settings", 12, 3, oled_buf);
      SSD1306_display(oled_buf);
      break;
    case 3:
      SSD1306_string(10, 20, "1) Meas.", 12, 1, oled_buf);
      SSD1306_string(10, 30, "2) Read from SD", 12, 1, oled_buf);
      SSD1306_string(10, 40, "3) Settings", 12, 0, oled_buf);
      SSD1306_display(oled_buf);
      break;
    }
  }
  void printOLED()
  {
    while (true)
    {
      PrintHall();
      if (digitalRead(buttonPin4) == HIGH)
      {
        SSD1306_clear(oled_buf);
        break;
      }
    }
    Menu();
  }
  void PrintHall()
  {
    delay(500);
    SSD1306_clear(oled_buf);
    char GainTab[2];
    String StrGain = String(GainAct);
    StrGain.toCharArray(GainTab, 2);
    SSD1306_string(10, 20, GainTab, 12, 3, oled_buf);
    mlx.readData(data);

    String mojStrST = String(data.StatusX);
    char strStatus[8];
    mojStrST.toCharArray(strStatus, 8);
    SSD1306_string(10, 50, "X/Y: ", 12, 3, oled_buf);
    SSD1306_string(30, 50, strStatus, 12, 3, oled_buf);

    String mojStrSTZ = String(data.StatusY);
    char strStatusY[8];
    mojStrSTZ.toCharArray(strStatusY, 8);
    SSD1306_string(70, 50, " Z: ", 12, 3, oled_buf);
    SSD1306_string(90, 50, strStatusY, 12, 3, oled_buf);

    String mojStrTemp = String(data.x - Xcon);
    char strT[8];
    mojStrTemp.toCharArray(strT, 8);
    SSD1306_string(20, 20, "X: ", 12, 3, oled_buf);
    SSD1306_string(40, 20, strT, 12, 3, oled_buf);
    SSD1306_string(100, 20, "uT", 12, 3, oled_buf);
    String mojStrPr = String(data.y - Ycon);
    char strP[8];
    mojStrPr.toCharArray(strP, 8);
    SSD1306_string(20, 30, "Y: ", 12, 3, oled_buf);
    SSD1306_string(40, 30, strP, 12, 3, oled_buf);
    SSD1306_string(100, 30, "uT", 12, 3, oled_buf);
    String mojStrH = String(data.z - Zcon);
    char strH[8];
    mojStrH.toCharArray(strH, 8);
    SSD1306_string(20, 40, "Z: ", 12, 3, oled_buf);
    SSD1306_string(40, 40, strH, 12, 3, oled_buf);
    SSD1306_string(100, 40, "uT", 12, 3, oled_buf);
    if (digitalRead(buttonPin3) == HIGH)
    {
      plikSD = SD.open("opis.txt", FILE_WRITE);
      delay(100);
      SSD1306_string(60, 0, "Data saved", 12, 0, oled_buf);
      delay(500);
      plikSD.print(strT);
      plikSD.print("\t");
      plikSD.print(strP);
      plikSD.print("\t");
      plikSD.print(strH);
      plikSD.print("\n");
      plikSD.close();
    }
    else if (digitalRead(buttonPin1) == HIGH)
    {
      ZeroAxis();
    }
    else if (digitalRead(buttonPin2) == HIGH)
    {
      AxisNormal();
    }
    SSD1306_display(oled_buf);
  }
  void AxisNormal()
  {
    Xcon = 0;
    Ycon = 0;
    Zcon = 0;
  }
  void ZeroAxis()
  {
    delay(3000);
    float TabXAxis;
    float TabYAxis;
    float TabZAxis;
    for (int i = 0; i < 2000; i++)
    {
      mlx.readData(data);
      TabXAxis += data.x;
      TabYAxis += data.y;
      TabZAxis += data.z;
    }
    Xcon = TabXAxis / 2000;
    Ycon = TabYAxis / 2000;
    Zcon = TabZAxis / 2000;
  }
  void setup()
  {
    pinMode(10, OUTPUT);
    SD.begin();
    delay(100);
    plikSD = SD.open("opis.txt", FILE_WRITE);
    plikSD.close();
    delay(100);
    Serial.begin(9600);
    SSD1306_begin();
    SSD1306_clear(oled_buf);
    SSD1306_display(oled_buf);
    delay(2000);
    SSD1306_clear(oled_buf);
    Wire.begin();
    StartUpHall();
    Menu();
  }
  void Gain()
  {
    {
      byte state = 1;
      ManuGain(1);
      while (true)
      {

        delay(200);
        if (digitalRead(buttonPin1) == HIGH)
        {
          if (state < 3)
            state++;
          else
            state = 1;
          ManuGain(state);
        }
        else if (digitalRead(buttonPin2) == HIGH)
        {
          if (state < 1)
            state = 3;
          else
            state--;
          ManuGain(state);
        }
        else if (digitalRead(buttonPin4) == HIGH)
          break;
        else if (digitalRead(buttonPin3) == HIGH)
        {
          if (state == 1)
          {
            mlx.setGainSel(7);
            mlx.GainValue = 7;
            break;
          }
          if (state == 2)
          {
            mlx.setGainSel(4);
            mlx.GainValue = 4;
            break;
          }
          if (state == 3)
          {
            mlx.setGainSel(2);
            mlx.GainValue = 2;
            break;
          }
        }
      }
      Settings();
    }
  }
  void ManuGain(int setGain)
  {
    SSD1306_clear(oled_buf);
    SSD1306_string(50, 0, "GAIN", 12, 0, oled_buf);
    switch (setGain)
    {
    case 1:
      SSD1306_string(10, 20, "1) x1", 12, 0, oled_buf);
      SSD1306_string(10, 30, "2) x2", 12, 3, oled_buf);
      SSD1306_string(10, 40, "3) x3", 12, 3, oled_buf);
      SSD1306_display(oled_buf);
      break;
    case 2:
      SSD1306_string(10, 20, "1) x1", 12, 3, oled_buf);
      SSD1306_string(10, 30, "2) x2", 12, 0, oled_buf);
      SSD1306_string(10, 40, "3) x3", 12, 3, oled_buf);
      SSD1306_display(oled_buf);
      break;
    case 3:
      SSD1306_string(10, 20, "1) x1", 12, 3, oled_buf);
      SSD1306_string(10, 30, "2) x2", 12, 3, oled_buf);
      SSD1306_string(10, 40, "3) x3", 12, 0, oled_buf);
      SSD1306_display(oled_buf);
      break;
    }
  }
  void Res()
  {
    {
      byte state = 1;
      ManuRes(1);
      while (true)
      {
        delay(200);
        if (digitalRead(buttonPin1) == HIGH)
        {
          if (state < 3)
            state++;
          else
            state = 1;
          ManuRes(state);
        }
        else if (digitalRead(buttonPin2) == HIGH)
        {
          if (state < 1)
            state = 3;
          else
            state--;
          ManuRes(state);
        }
        else if (digitalRead(buttonPin4) == HIGH)
          break;
        else if (digitalRead(buttonPin3) == HIGH)
        {
          if (state == 1)
          {
            mlx.setResolution(0, 0, 0);
            mlx.base_xy_sens_hc0xc = mlx.resValues[0][0];
            mlx.base_z_sens_hc0xc = mlx.resValues[0][1];
            break;
          }
          if (state == 2)
          {
            mlx.setResolution(1, 1, 1);
            mlx.base_xy_sens_hc0xc = mlx.resValues[1][0];
            mlx.base_z_sens_hc0xc = mlx.resValues[1][1];
            break;
          }
          if (state == 3)
          {
            mlx.setResolution(2, 2, 2);
            mlx.base_xy_sens_hc0xc = mlx.resValues[2][0];
            mlx.base_z_sens_hc0xc = mlx.resValues[2][1];
            break;
          }
        }
      }
      Settings();
    }
  }
  void ManuRes(int setRes)
  {
    SSD1306_clear(oled_buf);
    SSD1306_string(40, 0, "Resolution", 12, 0, oled_buf);
    switch (setRes)
    {
    case 1:
      SSD1306_string(10, 20, "1) Res = 0", 12, 0, oled_buf);
      SSD1306_string(10, 30, "2) Res = 1", 12, 3, oled_buf);
      SSD1306_string(10, 40, "3) Res = 2", 12, 3, oled_buf);
      SSD1306_display(oled_buf);
      break;
    case 2:
      SSD1306_string(10, 20, "1) Res = 0", 12, 3, oled_buf);
      SSD1306_string(10, 30, "2) Res = 1", 12, 0, oled_buf);
      SSD1306_string(10, 40, "3) Res = 2", 12, 3, oled_buf);
      SSD1306_display(oled_buf);
      break;
    case 3:
      SSD1306_string(10, 20, "1) Res = 0", 12, 3, oled_buf);
      SSD1306_string(10, 30, "2) Res = 1", 12, 3, oled_buf);
      SSD1306_string(10, 40, "3) Res = 2", 12, 0, oled_buf);
      SSD1306_display(oled_buf);
      break;
    }
  }
  void loop()
  {
  }