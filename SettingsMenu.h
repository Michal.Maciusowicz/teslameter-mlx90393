#include "D:\Pulpit\teslameter-mlx90393\LibUsed.h"
void Settings()
{
  SSD1306_clear(oled_buf);
  byte state = 1;
  MenuSettings(1);
  SSD1306_string(60, 0, "SETTINGS", 12, 0, oled_buf);
  while (true)
  {
    
    delay(200);
    if (digitalRead(buttonPin1) == HIGH)
    {
      if (state < 2)
        state++;
      else
        state = 1;
      MenuSettings(state);
    }
    else if (digitalRead(buttonPin2) == HIGH)
    {
      if (state < 1)
        state = 2;
      else
        state--;
      MenuSettings(state);
    }
    else if(digitalRead(buttonPin4) == HIGH)
      break;
  }
  Menu();
}
void MenuSettings(int setStatus)
{
  SSD1306_clear(oled_buf);
  switch(setStatus)
  {
    case 1:
      SSD1306_string(10, 20, "1) Gain" , 12, 0, oled_buf);
      SSD1306_string(10, 30, "2) Resolution" , 12, 3, oled_buf);
      SSD1306_display(oled_buf);
      break;
    case 2:
      SSD1306_string(10, 20, "1) Gain" , 12, 3, oled_buf);
      SSD1306_string(10, 30, "2) Resolution" , 12, 0, oled_buf);
      SSD1306_display(oled_buf);
      break;
  }
}